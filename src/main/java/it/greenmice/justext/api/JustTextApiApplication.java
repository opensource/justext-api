package it.greenmice.justext.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JustTextApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(JustTextApiApplication.class, args);
	}

}
