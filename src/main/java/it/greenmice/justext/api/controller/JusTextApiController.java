package it.greenmice.justext.api.controller;

import nl.wizenoze.justext.ClassifierProperties;
import nl.wizenoze.justext.JusText;
import nl.wizenoze.justext.paragraph.Paragraph;
import nl.wizenoze.justext.util.StopWordsUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;


@RestController
public class JusTextApiController {

    private final Logger log = LoggerFactory.getLogger(JusTextApiController.class);

    @GetMapping("/api/justext")
    public ResponseEntity<List<Paragraph>> jusTextUrl(
            @RequestParam String url,
            @RequestParam(defaultValue = "it") String language,
            @RequestParam(required = false, defaultValue = "200") int lengthHigh,
            @RequestParam(required = false, defaultValue = "70") int lengthLow,
            @RequestParam(required = false, defaultValue = "200") int maxHeadingDistance,
            @RequestParam(required = false, defaultValue = "0.2") BigDecimal maxLinkDensity,
            @RequestParam(required = false, defaultValue = "false") boolean noHeadings,
            @RequestParam(required = false, defaultValue = "false") boolean noHeadlines,
            @RequestParam(required = false, defaultValue = "true") boolean noImages,
            @RequestParam(required = false, defaultValue = "0.32") BigDecimal stopWordsHigh,
            @RequestParam(required = false, defaultValue = "0.30") BigDecimal stopWordsLow
    ) throws IOException {

        if (url.isEmpty()) {
            // TODO return JSON error.
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        ClassifierProperties.Builder classifierProperties = new ClassifierProperties.Builder();
        classifierProperties.setLengthHigh(lengthHigh);
        classifierProperties.setLengthLow(lengthLow);
        classifierProperties.setMaxHeadingDistance(maxHeadingDistance);
        classifierProperties.setMaxLinkDensity(maxLinkDensity);
        classifierProperties.setNoHeadings(noHeadings);
        classifierProperties.setNoHeadlines(noHeadlines);
        classifierProperties.setNoImages(noImages);
        classifierProperties.setStopWordsHigh(stopWordsHigh);
        classifierProperties.setStopWordsLow(stopWordsLow);
        JusText jusText = new JusText(classifierProperties.build());
        Document document = Jsoup.connect(url).get();
        Set<String> stopWords = StopWordsUtil.getStopWords(language);
        List<Paragraph> paragraphs = jusText.extract(document.html(), stopWords);
        return new ResponseEntity<>(paragraphs, HttpStatus.OK);
    }

    @PostMapping("/api/justext")
    public ResponseEntity<List<Paragraph>> jusTextHtml(
            @RequestParam(defaultValue = "it") String language,
            @RequestParam(required = false, defaultValue = "200") int lengthHigh,
            @RequestParam(required = false, defaultValue = "70") int lengthLow,
            @RequestParam(required = false, defaultValue = "200") int maxHeadingDistance,
            @RequestParam(required = false, defaultValue = "0.2") BigDecimal maxLinkDensity,
            @RequestParam(required = false, defaultValue = "false") boolean noHeadings,
            @RequestParam(required = false, defaultValue = "false") boolean noHeadlines,
            @RequestParam(required = false, defaultValue = "true") boolean noImages,
            @RequestParam(required = false, defaultValue = "0.32") BigDecimal stopWordsHigh,
            @RequestParam(required = false, defaultValue = "0.30") BigDecimal stopWordsLow,
            @RequestBody(required = true) Map<String, String> content
    ) throws IOException {
        ClassifierProperties.Builder classifierProperties = new ClassifierProperties.Builder();
        classifierProperties.setLengthHigh(lengthHigh);
        classifierProperties.setLengthLow(lengthLow);
        classifierProperties.setMaxHeadingDistance(maxHeadingDistance);
        classifierProperties.setMaxLinkDensity(maxLinkDensity);
        classifierProperties.setNoHeadings(noHeadings);
        classifierProperties.setNoHeadlines(noHeadlines);
        classifierProperties.setNoImages(noImages);
        classifierProperties.setStopWordsHigh(stopWordsHigh);
        classifierProperties.setStopWordsLow(stopWordsLow);
        JusText jusText = new JusText(classifierProperties.build());
        Set<String> stopWords = StopWordsUtil.getStopWords(language);
        List<Paragraph> paragraphs = jusText.extract(content.get("html"), stopWords);
        return new ResponseEntity<>(paragraphs, HttpStatus.OK);
    }
}
